class ProductsController < ApplicationController
  before_action :find_product, only: %i[edit update destroy show category]

  def show
    @questions = Question.all
  end

  def category; end

  def edit; end

  private

  def find_product
    @product = Product.friendly.find(params[:id])
  end
end
