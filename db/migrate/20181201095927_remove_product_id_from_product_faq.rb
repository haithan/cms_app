class RemoveProductIdFromProductFaq < ActiveRecord::Migration[5.1]
  def change
    remove_reference :product_faqs, :product, index: true, foreign_key: true
  end
end
