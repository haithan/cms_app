PRODUCT_TYPE_PARENT = 1
PRODUCT_TYPE_CHILD = 2

MALE = 1
FEMALE = 0

GENDER_ARRAY = [
  ['Male', MALE],
  ['Female', FEMALE]
].freeze

# SEO

TITLE = 'Wecare'.freeze
DESCRIPTION = 'Bảo hiểm uy tín'.freeze
KEYWORDS = 'Bảo hiểm, Bảo hiểm tai nạn, Bảo hiểm ung thư'.freeze