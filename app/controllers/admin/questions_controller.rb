class Admin::QuestionsController < ApplicationController
  before_action :authenticate_user!
  before_action :find_question, only: %i[edit update destroy]
  layout 'admin'

  def index
    @questions = Question.all
  end

  def new
    @question = Question.new
  end

  def create
    @question = Question.new(question_params)
    if @question.save!
      flash[:notice] = 'Successfully created question!'
      redirect_to admin_questions_path
    else
      flash[:alert] = 'Error creating new question!'
      render :new
    end
  end

  def edit; end

  def update
    if @question.update_attributes(question_params)
      flash[:notice] = 'Successfully updated question!'
      redirect_to admin_question_path(@question)
    else
      flash[:alert] = 'Error updating question!'
      render :edit
    end
  end

  def destroy
    if @question.destroy
      flash[:notice] = 'Successfully deleted question!'
      redirect_to admin_questions_path
    else
      flash[:alert] = 'Error updating question!'
    end
  end

  private

  def question_params
    params.require(:question).permit(:question)
  end

  def find_question
    @question = Question.find(params[:id])
  end
end
