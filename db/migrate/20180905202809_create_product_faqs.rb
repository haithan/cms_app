class CreateProductFaqs < ActiveRecord::Migration[5.1]
  def change
    create_table :product_faqs do |t|
      t.text :question
      t.text :answer
      t.references :product, foreign_key: { to_table: :products }
      t.timestamps
    end
  end
end
