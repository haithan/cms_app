class CreateSlideCustoms < ActiveRecord::Migration[5.1]
  def change
    create_table :slide_customs do |t|
      t.string :image_url
      t.string :introduce_title
      t.string :introduce_content
      t.integer :product_id
      t.string :custom_url
      t.timestamps
    end
  end
end
