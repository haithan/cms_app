class BlogController < ApplicationController
  protect_from_forgery with: :exception
  before_action :find_post, only: %i[show]

  def index
    @posts = Post.all.paginate(page: params[:page], per_page: 9)
  end

  def show; end

  private

  def find_post
    @post = Post.find(params[:id])
  end
end
