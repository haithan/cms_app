require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module CmsApp
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.1

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
    config.assets.paths << Rails.root.join('app', 'assets', 'fonts')
    config.action_view.sanitized_allowed_tags = %w[strong em a meta h1 title]
    config.action_view.sanitized_allowed_attributes = %w[href title property content]
    config.autoload_paths += %w(#{config.root}/app/models/ckeditor)
    # config.active_job.queue_adapter = :sidekiq
  end
end
