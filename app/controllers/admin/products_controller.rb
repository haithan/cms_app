class Admin::ProductsController < ApplicationController
  before_action :authenticate_user!
  before_action :find_product, only: %i[edit update destroy show]
  layout 'admin'

  def index
    @products = Product.where(product_type: [PRODUCT_TYPE_PARENT, nil])
  end

  def new
    @product = Product.new
  end

  def create
    ActiveRecord::Base.transaction do
      product = Product.new(product_params)
      if product.save!
        if params[:product][:parent_id]
          product.product_type = PRODUCT_TYPE_CHILD
          product.save!

          ProductConnection.create!(
            parent_product_id: Product.friendly.find(params[:product][:parent_id]).id,
            child_product_id: product.id
          )
        end

        flash[:notice] = 'Successfully created product!'
        redirect_to admin_products_path
      else
        flash[:alert] = 'Error creating new product!'
        render :new
      end
    end
  end

  def show; end

  def edit; end

  def update
    ActiveRecord::Base.transaction do
      begin
        @product.update!(
          name: product_params[:name],
          banner_image: product_params[:banner_image],
          slogan: product_params[:slogan],
          first_benefit: product_params[:first_benefit],
          first_benefit_slogan: product_params[:first_benefit_slogan],
          second_benefit: product_params[:second_benefit],
          second_benefit_slogan: product_params[:second_benefit_slogan],
          third_benefit: product_params[:third_benefit],
          third_benefit_slogan: product_params[:third_benefit_slogan]
        )

        flash[:notice] = 'Successfully updated product!'
        redirect_to admin_product_path(@product)
      rescue ActiveRecord::RecordInvalid
        flash[:alert] = 'Error updating product!'
        render :edit
      end
    end
  end

  def destroy
    if @product.destroy
      flash[:notice] = 'Successfully deleted question!'
      redirect_to admin_products_path
    else
      flash[:alert] = 'Error updating question!'
    end
  end

  def new_child_product
    @product = Product.new
    render :new
  end

  private

  # def product_params
  #   params.require(:product).permit(:name, :description, :meta_desc, :thumbnail)
  # end

  def product_params
    params.require(:product).permit(
      :name,
      :title, :title_tag,
      :description, :thumbnail,
      :meta_desc, :meta_robot, :meta_keys,
      :h1_tag,
      :sec_1_title, :sec_1_content,
      :sec_2_title, :sec_2_content,
      :sec_3_title, :sec_3_content
    )
  end

  def faq_params
    params.require(:product).permit(faq: %i[question answer id])
  end

  def find_product
    @product = Product.friendly.find(params[:id])
  end
end
