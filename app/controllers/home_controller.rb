class HomeController < ApplicationController
  protect_from_forgery with: :exception

  def index
    @questions = Question.all
    @products = Product.where(product_type: PRODUCT_TYPE_PARENT)
  end
end
