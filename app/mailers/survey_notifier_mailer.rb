class SurveyNotifierMailer < ApplicationMailer

  def send_email_after_feedback(feedback)
    @feedback = feedback
    mail to: 'tuyen055@gmail.com', subject: 'New feedback'
  end

  # send a signup email to the user, pass in the user object that   contains the user's email address
end
