class CreateFeedbacks < ActiveRecord::Migration[5.1]
  def change
    create_table :feedbacks do |t|
      t.boolean :is_checked, default: false
      t.string :customer
      t.string :phone_number
      t.timestamps
    end
  end
end
