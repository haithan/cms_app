var new_translation_block = '       <div class="form-group">\
        <input class="form-control" id="product_faq_question" name="product[faq[]question]" type="text" placeholder="Question" required>\
        <textarea class="form-control" id="product_faq_answer" name="product[faq[]answer]" placeholder="Answer" required></textarea>\
      </div> \
  <button type="button" class="btn btn-danger btn__faq--removal">Remove</button> \
';

$(document).on("click", function(evt) {
  if (evt.target.classList.contains("btn__faq--removal")) {
    $(evt.target).closest("div").remove()
    evt.preventDefault();
  } else if (evt.target.classList.contains("btn__faq-add")) {
    var d = document.createElement("div");
    d.className = "faq_container";
    d.innerHTML = new_translation_block;
    $("#add_faq")[0].before(d);
    evt.preventDefault();
  }
});

$(function() { 
  init_meta_desc();
  init_meta_robot();
  init_meta_keys();
  init_h1_tag();
  init_title_tag();
}); 

function init_meta_desc() {
  var editor_meta_desc = ace.edit("meta_desc");
  editor_meta_desc.setTheme("ace/theme/xcode");
  editor_meta_desc.session.setMode("ace/mode/html");
  editor_meta_desc.setOption({
    cursorStyle: "slim"
  });
  var textarea_meta_desc = $('#textarea_meta_desc');
  editor_meta_desc.setValue(textarea_meta_desc.val());
  editor_meta_desc.getSession().on("change", function () {
      textarea_meta_desc.val(editor_meta_desc.getSession().getValue());
  });
}

function init_meta_robot() {
  var editor_meta_robot = ace.edit("meta_robot");
  editor_meta_robot.setTheme("ace/theme/xcode");
  editor_meta_robot.session.setMode("ace/mode/html");
  editor_meta_robot.setOption({
    cursorStyle: "slim"
  });
  var textarea_meta_robot = $('#textarea_meta_robot');
  editor_meta_robot.setValue(textarea_meta_robot.val());
  editor_meta_robot.getSession().on("change", function () {
      textarea_meta_robot.val(editor_meta_robot.getSession().getValue());
  });
}

function init_meta_keys() {
  var editor_meta_keys = ace.edit("meta_keys");
  editor_meta_keys.setTheme("ace/theme/xcode");
  editor_meta_keys.session.setMode("ace/mode/html");
  editor_meta_keys.setOption({
    cursorStyle: "slim"
  });
  var textarea_meta_keys = $('#textarea_meta_keys');
  editor_meta_keys.setValue(textarea_meta_keys.val());
  editor_meta_keys.getSession().on("change", function () {
      textarea_meta_keys.val(editor_meta_keys.getSession().getValue());
  });
}

function init_h1_tag() {
  var editor_h1_tag = ace.edit("h1_tag");
  editor_h1_tag.setTheme("ace/theme/xcode");
  editor_h1_tag.session.setMode("ace/mode/html");
  editor_h1_tag.setOption({
    cursorStyle: "slim"
  });
  var textarea_h1_tag = $('#textarea_h1_tag');
  editor_h1_tag.setValue(textarea_h1_tag.val());
  editor_h1_tag.getSession().on("change", function () {
      textarea_h1_tag.val(editor_h1_tag.getSession().getValue());
  });
}

function init_title_tag() {
  var editor_title_tag = ace.edit("title_tag");
  editor_title_tag.setTheme("ace/theme/xcode");
  editor_title_tag.session.setMode("ace/mode/html");
  editor_title_tag.setOption({
    cursorStyle: "slim"
  });
  var textarea_title_tag = $('#textarea_title_tag');
  editor_title_tag.setValue(textarea_title_tag.val());
  editor_title_tag.getSession().on("change", function () {
      textarea_title_tag.val(editor_title_tag.getSession().getValue());
  });
}