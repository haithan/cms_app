class Admin::SlideCustomController < ApplicationController
  before_action :authenticate_user!
  before_action :find_slide, only: %i[edit update destroy]
  layout 'admin'

  def index
    @slides = SlideCustom.all
  end

  def show
    @feedback = Feedback.includes(answers: [:question])
                        .where(id: params[:id]).first
    return if @feedback.is_checked
    @feedback.update(is_checked: true)
    @feedback
  end

  private

  def find_slide
    @slide = SlideCustom.find(params[:id])
  end
end
