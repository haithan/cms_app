class Admin::FaqsController < ApplicationController
  before_action :authenticate_user!
  before_action :find_faq, only: %i[edit update destroy]
  layout 'admin'

  def index
    @product_faqs = ProductFaq.all
  end

  def new
    @product_faq = ProductFaq.new
  end

  def create
    @product_faq = ProductFaq.new(faq_params)
    if @product_faq.save!
      flash[:notice] = 'Successfully created!'
      redirect_to admin_faqs_path
    else
      flash[:alert] = 'Error!'
      render :new
    end
  end

  def edit; end

  def update
    if @product_faq.update_attributes(faq_params)
      flash[:notice] = 'Successfully updated!'
      redirect_to admin_faq_path(@product_faq)
    else
      flash[:alert] = 'Error updating!'
      render :edit
    end
  end

  def destroy
    if @product_faq.destroy
      flash[:notice] = 'Successfully deleted!'
      redirect_to admin_faqs_path
    else
      flash[:alert] = 'Error when destroying!'
    end
  end

  private

  def faq_params
    params.require(:product_faq).permit(:question, :answer)
  end

  def find_faq
    @product_faq = ProductFaq.find(params[:id])
  end
end
