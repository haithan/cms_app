class AddBirthdayAndGenderToFeedback < ActiveRecord::Migration[5.1]
  def change
    add_column :feedbacks, :gender, :integer
    add_column :feedbacks, :birthday, :date
  end
end
