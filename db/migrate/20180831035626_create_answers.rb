class CreateAnswers < ActiveRecord::Migration[5.1]
  def change
    create_table :answers do |t|
      t.references :question, foreign_key: { to_table: :questions }
      t.references :feedback, foreign_key: { to_table: :feedbacks }
      t.text :answer
      t.timestamps
    end
  end
end
