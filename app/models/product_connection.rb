class ProductConnection < ApplicationRecord
  belongs_to :parent_product,
             class_name: 'Product',
             foreign_key: 'parent_product_id'
end
