class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :load_products

  private

  def load_products
    @products = Product.where(product_type: [PRODUCT_TYPE_PARENT, nil])
  end
end
