class AddForeignKeyToFeedback < ActiveRecord::Migration[5.1]
  def change
    add_reference :feedbacks, :product, foreign_key: true
  end
end
