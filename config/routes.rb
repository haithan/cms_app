Rails.application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'
  require 'sidekiq/web'
  mount Sidekiq::Web => '/sidekiq'

  namespace :admin do
    resources :top
    resources :posts
    resources :questions, except: [:show]
    resources :feedbacks, only: %i[index show]
    resources :products
    resources :faqs
    get 'products/:id/new_child_product', to: 'products#new_child_product', as: 'new_child_product'
  end

  get :admin, to: 'admin/top#index'

  devise_for :users, path: 'devise', path_names: {
    sign_in: 'login',
    sign_out: 'logout',
    sign_up: 'register'
  } # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :users

  root to: 'home#index'
  resources :blog, only: %i[index show]
  resources :survey, only: %i[create]
  resources :products, only: %i[show]
  get 'category/:id', to: 'products#category', as: 'category'
  resources :faqs, only: %i[index]
  # get :blog, to: 'blog#index'
end
