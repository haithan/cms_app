class Admin::FeedbacksController < ApplicationController
  before_action :authenticate_user!
  before_action :find_question, only: %i[edit update destroy]
  layout 'admin'

  def index
    @feedbacks = Feedback.all
  end

  def show
    @feedback = Feedback.includes(answers: [:question])
                        .where(id: params[:id]).first
    return if @feedback.is_checked
    @feedback.update(is_checked: true)
    @feedback
  end
end
