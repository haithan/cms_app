json.extract! user, :id, :email, :from_locale, :to_locale, :created_at, :updated_at
json.url user_url(user, format: :json)
