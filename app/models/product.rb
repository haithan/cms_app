class Product < ApplicationRecord
  extend FriendlyId
  friendly_id :name, use: :slugged
  has_and_belongs_to_many :products,
                          join_table: :product_connections,
                          foreign_key: :parent_product_id,
                          association_foreign_key: :child_product_id

  has_many :from_products_connections,
           class_name: 'ProductConnection',
           foreign_key: 'child_product_id',
           dependent: :destroy

  has_many :from_products,
           through: 'from_products_connections',
           source: 'parent_product'

  has_many :feedbacks
  mount_uploader :thumbnail, ThumbnailUploader

  def child_product?
    product_type == PRODUCT_TYPE_CHILD
  end

  def parent_product?
    product_type == PRODUCT_TYPE_PARENT
  end
end
