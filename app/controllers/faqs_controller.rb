class FaqsController < ApplicationController
  protect_from_forgery with: :exception
  before_action :find_post, only: %i[show]

  def index
    @faqs = ProductFaq.all
  end

  def show; end
end
