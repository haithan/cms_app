class CreateProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :products do |t|
      t.string :name
      t.integer :product_type, default: 1
      t.integer :status, default: 0
      t.timestamps
    end
  end
end
