require 'faker'

namespace :cms_admin do
  desc 'Create user'
  task create_user: :environment do
    User.destroy_all
    User.create! name: 'Admin', email: 'admin@gmail.com',role: User::ADMIN,
                 password: '123456', password_confirmation: '123456'
  end

  desc 'Create dummy data'
  task create_question: :environment do
    Question.create! question: 'Question A'
    Question.create! question: 'Question B'
    Question.create! question: 'Question C'
    Question.create! question: 'Question D'
    Question.create! question: 'Question E'
    Question.create! question: 'Question F'
  end

  task create_blog: :environment do
    20.times do
      Post.create! title: Faker::Lorem.sentence,
                   body: Faker::Lorem.paragraph(50, true, 4)
    end
  end
end
