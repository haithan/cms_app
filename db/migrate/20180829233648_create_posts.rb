class CreatePosts < ActiveRecord::Migration[5.1]
  def change
    create_table :posts do |t|
      t.string :title
      t.text :body
      t.string :thumbnail_url, default: nil
      t.timestamps
    end
  end
end
