class CreateProductConnections < ActiveRecord::Migration[5.1]
  def change
    create_table :product_connections do |t|
      t.references :parent_product, foreign_key: { to_table: :products }
      t.references :child_product, foreign_key: { to_table: :products }
    end
  end
end
