class SurveyController < ApplicationController
  def index
    @questions = Question.all
  end

  def create
    if feedback_params[:product_id].blank?
      flash[:alert] = 'Please choose at least one product'
      return redirect_back(fallback_location: root_path)
    end

    if find_product(feedback_params[:product_id]).blank?
      flash[:alert] = 'Please choose at least one product'
      return redirect_back(fallback_location: root_path)
    end

    # birthday = birthday_params.values.join("/")

    @feedback = Feedback.new(
      customer: feedback_params[:full_name],
      phone_number: feedback_params[:phone_number],
      product_id: feedback_params[:product_id],
      gender: feedback_params[:gender],
      # birthday: birthday
    )

    if @feedback.save!
      # answer_params.each do |answer|
      #   Answer.create!(
      #     feedback_id: @feedback.id,
      #     question_id: answer[:question_id],
      #     answer: answer[:answer]
      #   )
      # end

      flash[:notice] = 'Submit successful!'
      SurveyNotifierMailer.send_email_after_feedback(@feedback).deliver_later
      redirect_to root_path
    else
      flash[:alert] = 'Something wrong! - Please contact with supporter'
      redirect_back(fallback_location: root_path)
    end
  end

  private

  def feedback_params
    params.require(:feedback)
          .permit(:full_name, :phone_number, :product_id, :gender)
  end

  def birthday_params
    params.require(:date)
          .permit(:day, :month, :year)
  end

  def answer_params
    params.require(:feedback)
          .permit(:full_name, answers: %i[question_id answer])['answers']
  end

  def find_product(prod_id)
    Product.where(id: prod_id).first
  end
end
