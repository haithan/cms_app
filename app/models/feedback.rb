class Feedback < ApplicationRecord
  has_many :answers, dependent: :destroy
  belongs_to :product
end
