class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # role constants
  ADMIN = 1
  ROLES = { ADMIN => 'Admin' }.freeze

  def admin?
    role == ADMIN
  end
end
