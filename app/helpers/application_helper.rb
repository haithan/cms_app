module ApplicationHelper
  # create navigation link in layouts/_nav
  def nav_link(link_text, link_path, params = {})
    class_name = current_page?(link_path) ? 'nav-item active' : 'nav-item'

    content_tag(:li, class: class_name) do
      link_to link_text, link_path, params
    end
  end

  def current_class?(test_path)
    return 'active' if request.path == test_path
    ''
  end

  def product_options
    options_for_select(Product.where(product_type: [PRODUCT_TYPE_PARENT, nil]).pluck(:name, :id))
  end

  def default_meta_title
    tag.meta(content: TITLE, property: 'title')
  end

  def default_meta_des
    tag.meta(content: DESCRIPTION, property: 'description')
  end

  def default_meta_keys
    tag.meta(content: KEYWORDS, property: 'keywords')
  end
end
