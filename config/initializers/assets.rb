# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'
Rails.application.config.assets.precompile += %w[main_styles.css responsive.css
                                                 news.css news_responsive.css
                                                 contact.css contact_responsive.css
                                                 animate.css owl.carousel.css
                                                 owl.theme.css
                                                 custom.js news_custom.js
                                                 contact_custom.js
                                                 ckeditor/* admin.js admin.scss
                                                 styles.css home.js]

# Add additional assets to the asset load path.
# Rails.application.config.assets.paths << Emoji.images_path
# Add Yarn node_modules folder to the asset load path.
Rails.application.config.assets.paths << Rails.root.join('node_modules')

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in the app/assets
# folder are already added.
# Rails.application.config.assets.precompile += %w( admin.js admin.css )
