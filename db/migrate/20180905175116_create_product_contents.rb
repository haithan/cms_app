class CreateProductContents < ActiveRecord::Migration[5.1]
  def change
    create_table :product_contents do |t|
      t.string :banner_image
      t.string :slogan
      t.string :first_benefit
      t.text :first_benefit_slogan
      t.string :second_benefit
      t.text :second_benefit_slogan
      t.string :third_benefit
      t.text :third_benefit_slogan
      t.references :product, foreign_key: { to_table: :products }
      t.timestamps
    end
  end
end
