 class UpdateProductContentTableStruct < ActiveRecord::Migration[5.1]
  def change
    add_column :products, :title, :string
    add_column :products, :title_tag, :string
    add_column :products, :description, :text
    add_column :products, :thumbnail, :text
    add_column :products, :meta_desc, :text
    add_column :products, :meta_robot, :text
    add_column :products, :meta_keys, :text
    add_column :products, :h1_tag, :string
    add_column :products, :sec_1_title, :text
    add_column :products, :sec_1_content, :text
    add_column :products, :sec_2_title, :text
    add_column :products, :sec_2_content, :text
    add_column :products, :sec_3_title, :text
    add_column :products, :sec_3_content, :text

    drop_table :product_contents
  end
end
